// This is the code for the client application
// This does not contain the solution to the second assignment 

function createBookBox(book) {
    const lista = document.getElementById("listofbooks")
    
    //creating all elements
    const li = document.createElement("LI")
    const bookboxElement = document.createElement("DIV")
    const imgElement = document.createElement("IMG")
    const titleElement = document.createElement("H2")
    const authorElement = document.createElement("H3")
    const starElement = document.createElement("DIV")
    const ystarElement = document.createElement("SPAN")
    const wstarElement = document.createElement("SPAN")
    const buttonAD = document.createElement("BUTTON")
    const nRating = document.createElement("SPAN")
    
    // set attributes where necassary 
    // id of li
    li.id = "li_" + book.title
    /// class of divBookbox
    cl = document.createAttribute("class")
    cl.value = "bookbox"
    bookboxElement.setAttributeNode(cl)
    /// source of img
    sc = document.createAttribute("src")
    sc.value = book.image
    imgElement.setAttributeNode(sc)
    /// class of stars
    ycl = document.createAttribute("class")
    ycl.value = "star yellowstar"
    ystarElement.setAttributeNode(ycl)
    wcl = document.createAttribute("class")
    wcl.value = "star"
    wstarElement.setAttributeNode(wcl)
    /// class of button

    function wishlist(){
        titleWL = document.createElement("P")
        title_to_add = book.title
        titleWL.id = "id_" + title_to_add
        titleWL.textContent =  title_to_add;
        if (this.textContent  ==  "Add to Wishlist"){
        this.textContent = "Remove from Wishlist"
        sidebar.append(titleWL)
        }
        else {
            
        titleWL_id = "id_" + title_to_add
        titleWL2 = document.getElementById("titleWL_id")
        document.getElementById("id_"+book.title).parentNode.removeChild(document.getElementById("id_"+book.title))
        this.textContent = "Add to Wishlist"
        }
    }
    
    buttonAD.id = "button_" + book.title
    buttonAD.addEventListener("click", wishlist)
    //id for title
    til = document.createAttribute("class")
    til.value = "class_title"
    titleElement.setAttributeNode(til)

    
    // fill content
    /// fill author and title
    titleElement.textContent = book.title;
    authorElement.textContent = book.authors;
    buttonAD.textContent = "Add to Wishlist"

    /// fill starselemnt 
    let rating = book.rating
    let ystartext = ""
    let wstartext = ""
    for (let i = 1; i < rating+1; i++){
        ystartext = ystartext +"★"
    }
    for (let i = 1; i < 6-rating; i++){
        wstartext = wstartext +"★"
    }
    ystarElement.textContent = ystartext
    wstarElement.textContent = wstartext
    nRating.textContent = ` (${book.numberrating})`

    // append all elements to respective mother element 
    starElement.append(ystarElement)
    starElement.append(wstarElement)
    starElement.append(nRating)
    bookboxElement.append(imgElement)
    bookboxElement.append(titleElement)
    bookboxElement.append(authorElement)
    bookboxElement.append(starElement)
    bookboxElement.append(buttonAD)
    li.append(bookboxElement)
  
    // return the box
    return li
    
}



function fillBooks(books) {
    booksobj = books
    console.log(books)
    const lista = document.getElementById("listofbooks")
    for (const idx in books) {
        const li = createBookBox(books[idx])
        lista.append(li)
    }
}


function filterBooks(books){
    searchq = document.getElementById("searchbox1").value.toUpperCase()
    //if (searchq){
        for( let book of booksobj ) {
            const li = document.getElementById("li_" + book.title)
            li.style.display = ""
            console.log(book.title + "a")
            const bookdata = (book.title + book.authors).toUpperCase()
            //const li = document.getElementById("li_" + book.title)
            if (bookdata.includes(searchq)) {
              li.style.display = "";
            } else {
              li.style.display = "none"
            }
          }
        }


function loadAndFillBooks(search) {
    const query = search != undefined?`?search=${search}`:""

    fetch('books.json'+query)
    .then(data => data.json())
    .then(books => { fillBooks(books) })
}

function addNewBook() {
    fetch("/api/books", {
        method: "POST",
        headers: {
            'content-type':'application/json;charset=utf-8'
        },
        body: JSON.stringify({title:"Bla"})
    })
}


function installOtherEventHandlers() {
    // Events to open and close menus
    const toggleitem = document.getElementsByClassName('menuitem')
    const hiddentoggle = document.getElementsByClassName('submenu')
    for (let i = 0; i<3; i++) {
        function clickHandler(){
            hiddentoggle[i].classList.toggle("hidden");
            };
        toggleitem[i].addEventListener("click", clickHandler);
        };
    };
    // Add wishlist

    
    // Events to call loadAndFillBooks with a new search value
    sbtn = document.getElementById("searchbutton")
    sbtn.addEventListener("click", filterBooks)


window.onload = () => {
    loadAndFillBooks() // If no parameter is given, search is undefined
    ;

    installOtherEventHandlers();
    

}



